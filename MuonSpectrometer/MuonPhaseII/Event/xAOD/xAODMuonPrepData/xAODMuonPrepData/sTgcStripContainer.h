/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODMUONPREPDATA_STGCSTRIPCONTAINER_H
#define XAODMUONPREPDATA_STGCSTRIPCONTAINER_H

#include "xAODMuonPrepData/sTgcStripClusterFwd.h"
#include "xAODMuonPrepData/sTgcStripCluster.h"
#include "xAODCore/CLASS_DEF.h"

namespace xAOD{
   using sTgcStripContainer_v1 = DataVector<sTgcStripCluster_v1>;
   using sTgcStripContainer = sTgcStripContainer_v1;
}
// Set up a CLID for the class:
CLASS_DEF( xAOD::sTgcStripContainer , 1114862428 , 1 )

#endif